from pandascout import pds

# change the my_path_with_parquet and my_single_parquet_name accordingly
my_path_with_parquet = '/eos/home-s/sterbini/2021/pandascout/examples/data/'
my_single_parquet_name = '2021.03.17.12.12.36.065000.parquet'

# loading the parquet data from a folder on a pandas DF
my_df_1 = pds.read_parquet(my_path_with_parquet)

# loading the parquet data from a folder, with some specific columns
my_df_2 = pds.read_parquet(my_path_with_parquet, columns=['BR.QFO/LOG.OASIS.I_MEAS','BR.QDE/LOG.OASIS.I_MEAS'])

# loading the parquet data from a specific file, with some specific columns
my_df_3 = pds.read_parquet(my_path_with_parquet + my_single_parquet_name, columns=['BR.QFO/LOG.OASIS.I_MEAS','BR.QDE/LOG.OASIS.I_MEAS'])

# getting the schema
my_df_2.get_schema()

# Each column of the DataFrame is dict with three keys (value, header and exception)
# One can get this keys directly on a new DF
my_df_2_value = my_df_2.get_value('BR.QFO/LOG.OASIS.I_MEAS')
my_df_2_header = my_df_2.get_header('BR.QFO/LOG.OASIS.I_MEAS')  
my_df_2_exception = my_df_2.get_exception('BR.QFO/LOG.OASIS.I_MEAS')  

# In addition we can select some specific columns 
my_df_2_value_selection = my_df_2.get_value('BR.QFO/LOG.OASIS.I_MEAS', ['CYCLE_TAG','INTERVAL_NS'])

# You can concatenate the data at your wish
my_df_4 = pds.concat([
my_df_2.get_value('BR.QFO/LOG.OASIS.I_MEAS',['CYCLE_TAG','INTERVAL_NS']),   
my_df_2.get_header('BR.QDE/LOG.OASIS.I_MEAS',['acqStamp'])], axis = 1)   

# You can save back to parquet
my_df_4.to_parquet('my_df_4.parquet')
