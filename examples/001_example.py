from pandascout import pds

main_folder = '/abp/examples/data_tirsi'

# reading also different schema
my_df_1 = pds.read_scout(main_folder)

# reading by columns
my_df_2 = pds.read_scout(main_folder, columns=['BR.QDE-ST/Samples','BR3.BPM/AcquisitionOrbit'])

# costumizing parquet file search /**/ means "all subfolders"
pds.read_scout(main_folder, glob_string='/**/2021.03.25.13.49*.parquet', columns=['BR.QDE-ST/Samples','BR3.BPM/AcquisitionOrbit'])

# not existing folders
pds.read_scout('/not_exist')

# comparing schama 
pds.compare_schema(main_folder)
