from setuptools import setup, find_packages

#########
# Setup #
#########

setup(
    name='pandascout',
    version='0.0.1',
    description='Extension of pandas for pyjapcscout parquet',
    url='',
    author='Guido Sterbini',
    packages=find_packages(),
    install_requires=[
        'numpy>=1.16.6',
        'pandas>=1.1.5',
        'pyarrow>=3.0.0',
        ]
    )
