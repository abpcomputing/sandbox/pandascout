## Description

This package is intended to provide additional methods the `pandas` 'DataFrame' and `Series` to easet eh handling of the `parquet` files produced by [pyjapcscout](https://gitlab.cern.ch/abpcomputing/sandbox/pyjapcscout).


## Install the package
To install this package
```
pip install git+https://gitlab.cern.ch/abpcomputing/sandbox/pandascout.git
```

on SWAN you should do 

```
pip install --user git+https://gitlab.cern.ch/abpcomputing/sandbox/pandascout.git
```

or
```
pip install --upgrade --user git+https://gitlab.cern.ch/abpcomputing/sandbox/pandascout.git
```

In any case **you need to change the SWAN path priority**
```
import sys
sys.path.insert(0, '/eos/user/<YOUR LOGIN INITIAL>/<YOUR LOGIN>/.local/lib/python<YOUR PYTHON VERSION>/site-packages')
sys.path
```

## Simple examples

Please refer to https://gitlab.cern.ch/abpcomputing/sandbox/pandascout/-/blob/master/examples/.

