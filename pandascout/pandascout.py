import numpy as np
import pandas as pd
import glob
import pyarrow.parquet as pq
import pathlib


pds = pd



def compare_schema(folder, reference_file = None):
    my_list = []
    a = sorted(glob.glob(folder+'/**/*.parquet', recursive=True))
    if len(a) > 0:
        print(f'Reference schema: {a[0]}')
        if reference_file == None:
            reference_file = a[0]
        ref_schema = pq.read_schema(reference_file)
        for ii in a:
            
            if not ref_schema==pq.read_schema(ii):
                print (f'\n ****** {ii} has a different schema! ****** ')
                print('This is the difference\n === Schema of the reference file ===')
                print(set(ref_schema)-set(pq.read_schema(ii)))
                print('This is the difference\n === Schema of the compared file ===')
                print(set(pq.read_schema(ii))-set(ref_schema))

                my_list.append(ii)
    else:
        print('No file in the folder.')
    return my_list

def read_scout(folder, columns=None, glob_string='/**/*.parquet', recursive=True, 
                 adding_source_filename=True, verbose=False, sort_index=True, sort_columns=True):
    '''
    Read the parquet file in a folder - recursively.
    We privileged usability to performance.

    Parameters
    ----------
    folder: string
       The main parquet folder.
    
    columns: list of strings
        The columns that will be read in the parquet. They need to exist in all parquets you want to read.
    
    glob_string: string
        The filtering strings for looking for the parquet files.
    
    recursive: boolean
        The recursive parameters of the glob.glob method.
    
    adding_source_filename: boolean
        To add the column "source file name" in the returned DataFrame.
        
    verbose: boolean
        To execute the method in the verbose mode.
        
    sort_index: boolean
        To sort the returned DataFrame indexes (datestamp).
    
    sort_columns: boolean
        To sort the returned DataFrame columns.
        
    Returns
    -------
    result: pandascout.DataFrame

    See also
    --------
    pandascout.DataFrame.get_value
    pandascout.DataFrame.get_index
    pandascout.DataFrame.get_exception
    '''
    a = sorted(glob.glob(folder + '/' + glob_string, recursive=recursive))
    my_list = [pd.DataFrame()]
    for ii in a:
        if verbose: print(ii)
        myDF=pd.read_parquet(ii, columns=columns)
        if adding_source_filename:
            myDF['# source file name'] = str(pathlib.Path('ii').parent.absolute()) + '/' + ii
        my_list.append(myDF)
    if sort_index: my_df = pd.concat(my_list).sort_index()
    if sort_columns: my_df = my_df.reindex(sorted(my_df.columns), axis=1)
    return DataFrame(my_df)


def read_pickleDict(file, indexing = 'cycleStamp'):
    '''This method is a safety net in case one needs to use the pickleDict format'''
    my_dict = pd.read_pickle(file)
    return spd.DataFrame(pd.read_pickle(file), index=[_getIndex(my_dict, indexing)])
    
def _getIndex(my_dict, indexing):
    '''Given a list of dictionaries it defines a single cycleStamp or acqStamp, corresponding to the np.max in the set'''
    return np.max([my_dict[0][ii]['header'][indexing] for ii in my_dict[0].keys()])

class DataFrame(pd.DataFrame):               
    '''This class is inheriting by pandas DataFrame and adding some additional function
    - get_value
    - get_header
    - get_exception
    - get_schema
    '''
    def _get_simpler_data(self, verbose = False):
        ''' This method is used for converting the bare dict from a pickleDict, 
        so it is needed only as safety net if one needs to use the pickleDict format
        and convert it back to parquet
        '''
        if verbose:
            print('################################')
            print(f'DataFrame: {len(self)} rows')
            print('################################\n')

        for ii in self.columns:
            aux=self[ii].dropna()
            if verbose:
                print(f'{ii}: {len(aux)} non-null rows')
            for jj in aux.iloc[0].keys():
                if verbose:
                    print(f'  - {jj}: {type(aux.iloc[0][jj])}')
                if type(aux.iloc[0][jj]) == dict:
                    for kk in aux.iloc[0][jj].keys():
                        if verbose:
                            print(f'    -- {kk}:\t\t {type(aux.iloc[0][jj][kk])}')
                        if type(aux.iloc[0][jj][kk]) == np.ndarray:
                            if verbose:
                                print('>>>> CASTING IT TO LIST!')
                            aux.iloc[0][jj][kk] = (aux.iloc[0][jj][kk]).tolist()
            if verbose:
                print()
    
    def get_value(self, column, key1 = None):
        '''Get the `value` key of the dict'''
        if key1 == None:
            key1 = self._get_keys(column, key = 'value')
            if key1 == None:
                return pd.DataFrame(self[column]._('value'))
    
        my_list = []
        if type(key1) == str:
            key1 = [key1]        
        assert type(key1) == list
        for ii in key1:
            my_list.append(pd.DataFrame(self[column]._('value')._(ii)))
        
        return pds.concat(my_list, axis = 1)
    
    def get_header(self, column, key1= None):
        '''Get the `header` key of the dict'''
        if key1 == None:
            key1 = self._get_keys(column, key = 'header')
            if key1 == None:
                return pd.DataFrame(self[column]._('header', separator = '##'))
        
        my_list = []
        if type(key1) == str:
            key1 = [key1]        
        assert type(key1) == list
        for ii in key1:
            my_list.append(pd.DataFrame(self[column]._('header', separator = '##')._(ii)))

        return pds.concat(my_list, axis = 1)
    
    def get_exception(self, column, key1= None):
        '''Get the `exceptions` key of the dictionary'''
        if key1 == None:
            key1 = self._get_keys(column, key = 'exception')
            if key1 == None:
                return pd.DataFrame(self[column]._('exception', separator = '##'))
            
        my_list = []
        if type(key1) == str:
            key1 = [key1]        
        assert type(key1) == list
        for ii in key1:
            my_list.append(pd.DataFrame(self[column]._('exception', separator = '##')._(ii)))
        
        return pds.concat(my_list, axis = 1)

    def get_schema(self):
        '''Give the schema of the df'''
        print('################################')
        print(f'DataFrame: {len(self)} rows')
        print('################################\n')

        for ii in self.columns:
            aux=self[ii].dropna()
            print(f'{ii}: {len(aux)} non-null rows')
            for jj in aux.iloc[0].keys():
                print(f'  - {jj}: {type(aux.iloc[0][jj])}')
                if type(aux.iloc[0][jj]) == dict:
                    for kk in aux.iloc[0][jj].keys():
                        print(f'    -- {kk}:\t\t {type(aux.iloc[0][jj][kk])}')
            print()
    
    def _get_keys(self, column, key):
        aux = self[column].dropna()
        if type(aux.iloc[0][key]) == dict:
            return list(aux.iloc[0][key].keys())
        else:
            return None
                
def _myFun(self,x,mykey):
    if x == None:
        return None
    else:
        return x[mykey]

def _key(self, mykey, separator = '#'):
    'Given a series of dict we can extract a key.'
    aux=self.apply(lambda x: _myFun(self,x,mykey)) 
    if not (mykey == "value"):
        aux.name = aux.name + separator + mykey
    return aux

pds.read_pickleDict = read_pickleDict
pds.Series._ = _key                 
pds.DataFrame = DataFrame         
pds.compare_schema = compare_schema
pds.read_scout = read_scout